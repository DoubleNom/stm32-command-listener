/**
 * @file    command_listener.cpp
 * @author  Paul Thomas
 * @date    8/12/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "command_listener.h"

#undef CRC
// https://github.com/d-bahr/CRCpp/blob/master/inc/CRC.h
#include <crc/crc.h>

CommandListener::CommandListener(UART_HandleTypeDef* uart, const char* tag) :
    logger(Logging::Logger::open(tag)), mUart(uart) {
  mFrame.reserve(bufferCapacity);
  HAL_UART_Receive_DMA(mUart, mBuffer.data(), bufferCapacity);
}

void CommandListener::run() {
  mBuffer.dmaCounter(__HAL_DMA_GET_COUNTER(mUart->hdmarx));
  while (mBuffer.size() != 0) {
    uint8_t c = mBuffer.read();
    logger->t("%c (%02x)", c, c);
    if (mChecksumCount != 0) {
      mAccumulatedChecksum |= c << (8 * (2 - mChecksumCount));
      --mChecksumCount;
      if (mChecksumCount != 0) { return; }
      if (mComputedChecksum != mAccumulatedChecksum) {
        logger->e("Checksum failure %04x != %04x", mComputedChecksum, mAccumulatedChecksum);
      } else {
        logger->t("%s", std::string(mFrame.begin(), mFrame.end()).c_str());
        mCommands.push(std::string { mFrame.begin(), mFrame.end() });
      }
      mFrame.clear();
      mComputedChecksum    = 0;
      mAccumulatedChecksum = 0;
    } else if (c == '\n') {
      if (mFrame.empty()) continue;
      mComputedChecksum = CRC::Calculate(&c, 1, CRC::CRC_16_ARC(), mComputedChecksum);
      mChecksumCount    = 2;
    } else if (std::isprint(c)) {
      if (mFrame.size() == bufferCapacity) {
        logger->e("buffer overflow %s", std::string(mFrame.begin(), mFrame.end()).c_str());
        mFrame.clear();
        mComputedChecksum = 0;
      }
      mComputedChecksum = CRC::Calculate(&c, 1, CRC::CRC_16_ARC(), mComputedChecksum);
      mFrame.push_back(c);
    }
  }
}

bool CommandListener::available() { return mCommands.size() != 0; }

std::string CommandListener::read() { return mCommands.read(); }

void CommandListener::clear() { mBuffer.clear(); }
