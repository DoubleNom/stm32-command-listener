/**
 * @file    command_listener.h
 * @author  Paul Thomas
 * @date    8/12/2023
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#ifndef DOUBLENOM_COMMAND_LISTENER_H
#define DOUBLENOM_COMMAND_LISTENER_H

#include <logging/logger.h>
#include <usart.h>

#include <circular_buffer/circular_buffer.hpp>
#include <string>

class CommandListener {
 public:
  static constexpr const char* DEFAULT_TAG = "CL";

 public:
  explicit CommandListener(UART_HandleTypeDef* uart, const char* tag = DEFAULT_TAG);
  void        run();
  bool        available();
  std::string read();
  void        clear();

 public:
  Logging::Logger* logger;

 private:
  static constexpr std::size_t bufferCapacity = 512;
  static constexpr std::size_t framesCapacity = 5;
  UART_HandleTypeDef*          mUart;
  CircularBuffer<uint8_t>      mBuffer { bufferCapacity };
  CircularBuffer<std::string>  mCommands { framesCapacity };
  std::vector<uint8_t>         mFrame               = {};
  uint8_t                      mChecksumCount       = 0;
  uint16_t                     mComputedChecksum    = 0;
  uint16_t                     mAccumulatedChecksum = 0;
};

#endif  // DOUBLENOM_COMMAND_LISTENER_H
